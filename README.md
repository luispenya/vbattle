# README #

vBattle is a small videogame intended to be used as AI research framework. Initially developed in Java currently it's been translated to C# and Unity3D 

### What is this repository for? ###

The vBattle is conceived as a tactical combat game, where two (or more) factions composed by several sets of warriors fight on a battlefield. 

It is part of the PhD. Thesis of Luis Peña [luis.penya@lurtis.com]
Documentation:
  
  Thesis: http://www.cetinia.urjc.es/sites/default/files/userfiles/file/publications/thesis_Luis.pdf


### Contribution guidelines ###


### Who do I talk to? ###

Development and Desing Luis Peña and Diego Fradejas

Some videos:
  https://www.youtube.com/watch?v=exqWQIFVzyY